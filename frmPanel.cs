﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace userContacts
{
    public partial class frmPanel : Form
    {
        public static User loggedIn;
        public static List<TipoComunicacao> Tcomunicacao;
        public frmPanel(User utilizador)
        {
            InitializeComponent();
            DAL ligacao = new DAL();

            loggedIn = utilizador;
            Helper.launchLogger("Login", loggedIn);
            Tcomunicacao = ligacao.getTipoComunicacoes();
            lblTitle.Text = string.Format(Helper.listaErros[8], utilizador.username);
            btnAddUser.Visible = loggedIn.Admin;
            btnAllUsers.Visible = loggedIn.Admin;
            btnMudarPassword.Visible = !loggedIn.Admin ? true : false; 

            Form f = new frmHome();
            foreach (Form frm in this.MdiChildren)
            {
                frm.Dispose();
                frm.Close();
            }
            f.MdiParent = this;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            f.Show();

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            if (Helper.getUserConfirmation(Helper.listaErros[9]))
            {
                this.Hide();
                frmLogin relog = new frmLogin();
                relog.ShowDialog();
                Helper.launchLogger("Logout", frmPanel.loggedIn);
                this.Dispose();
                this.Close();
            }
        }

        private void btnMeusContactos_Click(object sender, EventArgs e)
        {
            Form f = new frmMeusContactos();

            foreach (Form frm in this.MdiChildren)
            {
                frm.Dispose();
                frm.Close();
            }
            f.MdiParent = this;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            f.Show();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            Form f = new frmHome();

            foreach (Form frm in this.MdiChildren)
            {
                frm.Dispose();
                frm.Close();
            }
            f.MdiParent = this;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            f.Show();
        }

        private void BtnAddContacto_Click(object sender, EventArgs e)
        {
            Form f = new frmContacto(null);

            foreach (Form frm in this.MdiChildren)
            {
                frm.Dispose();
                frm.Close();
            }
            f.MdiParent = this;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            f.Show();
        }

        private void BtnAddUser_Click(object sender, EventArgs e)
        {
            Form f = new frmUser(null);

            foreach (Form frm in this.MdiChildren)
            {
                frm.Dispose();
                frm.Close();
            }
            f.MdiParent = this;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            f.Show();
        }

        private void BtnAllUsers_Click(object sender, EventArgs e)
        {
            Form f = new frmUserList();

            foreach (Form frm in this.MdiChildren)
            {
                frm.Dispose();
                frm.Close();
            }
            f.MdiParent = this;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            f.Show();
        }

        private void btnMudarPassword_Click(object sender, EventArgs e)
        {
            Form f = new frmMudarPass(loggedIn);

            foreach (Form frm in this.MdiChildren)
            {
                frm.Dispose();
                frm.Close();
            }
            f.MdiParent = this;
            f.Dock = DockStyle.Fill;
            f.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            f.Show();

        }
    }
}
