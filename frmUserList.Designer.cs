﻿namespace userContacts
{
    partial class frmUserList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataallUsers = new System.Windows.Forms.DataGridView();
            this.lblTitle = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataallUsers)).BeginInit();
            this.SuspendLayout();
            // 
            // dataallUsers
            // 
            this.dataallUsers.AllowUserToAddRows = false;
            this.dataallUsers.AllowUserToDeleteRows = false;
            this.dataallUsers.AllowUserToResizeRows = false;
            this.dataallUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataallUsers.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(255)))));
            this.dataallUsers.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataallUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataallUsers.Location = new System.Drawing.Point(323, 187);
            this.dataallUsers.Name = "dataallUsers";
            this.dataallUsers.ReadOnly = true;
            this.dataallUsers.RowHeadersVisible = false;
            this.dataallUsers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataallUsers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataallUsers.Size = new System.Drawing.Size(517, 150);
            this.dataallUsers.TabIndex = 0;
            this.dataallUsers.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataContactos_CellContentDoubleClick);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(100, 20);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(227, 20);
            this.lblTitle.TabIndex = 4;
            this.lblTitle.Text = "Doubleclick para abrir contacto";
            // 
            // frmUserList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.dataallUsers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUserList";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Panel";
            this.Resize += new System.EventHandler(this.FrmMeusContactos_ResizeEnd);
            ((System.ComponentModel.ISupportInitialize)(this.dataallUsers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataallUsers;
        private System.Windows.Forms.Label lblTitle;
    }
}