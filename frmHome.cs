﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace userContacts
{
    public partial class frmHome : Form
    {
        public frmHome()
        {
            InitializeComponent();
            resizeDataGrid();
            DAL ligacao = new DAL();
            List<Contacto> meus = ligacao.getPublicContacts();
            dataContactosPublicos.DataSource = meus;
            this.dataContactosPublicos.Columns["ID"].Visible = false;
            this.dataContactosPublicos.Columns["username"].Visible = false;
            this.dataContactosPublicos.Columns["modData"].Visible = false;
            this.dataContactosPublicos.Columns["publico"].Visible = false;
            this.dataContactosPublicos.Columns["modUsername"].Visible = false;
            this.dataContactosPublicos.Columns["dataCriacao"].Visible = false;

        }

        private void DataContactos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Contacto selecionado = (Contacto)dataContactosPublicos.CurrentRow.DataBoundItem;
            this.Hide();
            frmContacto single = new frmContacto(selecionado);
            single.MdiParent = this.MdiParent;
            single.Dock = DockStyle.Fill;
            single.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            single.Show();
            this.Dispose();
            this.Close();
        }

        private void resizeDataGrid()
        {
            dataContactosPublicos.Width = this.Width - 200;
            dataContactosPublicos.Height = this.Height - 100;
            dataContactosPublicos.Location = new Point(100, 50);
        }
        
        private void FrmMeusContactos_ResizeEnd(object sender, EventArgs e)
        {
            resizeDataGrid();
        }
    }
}
