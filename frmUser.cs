﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace userContacts
{
    public partial class frmUser : Form
    {
        private User currentUser;
        private bool novo;
        public frmUser(User selecionado = null)
        {
            InitializeComponent();
            currentUser = selecionado;

            iniciarUser();
            resizeDataGrid();
            iniciarCampos();
        }

        private void iniciarUser()
        {
            DAL ligacao = new DAL();
            if (currentUser == null)
            {
                currentUser = new User();
                iptUsername.Text = currentUser.username;
                iptisAdmin.Text = currentUser.Admin ? "True" : "False";
                novo = true;
            }
            else
            {
                ligacao.getUser(currentUser);
                iptUsername.Text = currentUser.username;
                iptisAdmin.Text = currentUser.Admin ? "True" : "False";
                novo = false;
            }
        }

        private void backHome()
        {
            this.Hide();
            frmUserList home = new frmUserList();
            home.MdiParent = this.MdiParent;
            home.Dock = DockStyle.Fill;
            home.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            home.Show();
            this.Dispose();
            this.Close();
        }

        private void resetLabels()
        {
            lblErrorsPwd1.Visible = false;
            lblErrorsPwd4.Visible = false;
            lblErrorsPwd3.Visible = false;
            lblErrorsPwd2.Visible = false;
            lblErrorsPwd5.Visible = false;
            lblErrorsGeral.Visible = false;

        }

        private void resizeDataGrid()
        {
            centerPanel.Width = this.Width - 200;
            centerPanel.Height = this.Height - 100;
            centerPanel.Location = new Point(100, 50);
            colUser.Width = colUser.Parent.Width / 3 * 2;
            colAdmin.Width = colAdmin.Parent.Width / 3;

            colPwd1.Width = colPwd1.Parent.Width / 2;
            colPwd2.Width = colPwd2.Parent.Width / 2;

            colToolbar.Width = colToolbar.Parent.Width;
        }

        private int validarCampos()
        {
            if (this.novo)
            {
                if (string.IsNullOrEmpty(iptUsername.Text) || string.IsNullOrEmpty(iptPasswordRepeat.Text)) { return 1; }
                if (!Helper.naotemEspacos(iptUsername.Text)) { return 3; }
                if (Helper.userExists(iptUsername.Text)) { return 4; }
            }
            if (!string.IsNullOrEmpty(iptPassword.Text))
            {
                if (iptPassword.Text != iptPasswordRepeat.Text || !Helper.validarComprimento(iptPassword.Text) || !Helper.validarDigitos(iptPassword.Text)) { return 2; }
            }
            return 0;
        }

        private void iniciarCampos()
        {
            bool canEdit = currentUser.username == frmPanel.loggedIn.username || frmPanel.loggedIn.Admin ? true : false;
            bool canDelete = frmPanel.loggedIn.Admin && currentUser.username != frmPanel.loggedIn.username && !novo ? true : false;

            iptUsername.ReadOnly = novo ? false : true ;
            btnSave.Visible = canEdit;
            btnSave.Text = novo ? "Adicionar" : "Guardar";
            btnDelete.Visible = canDelete;
        }

        private void FrmMeusContactos_ResizeEnd(object sender, EventArgs e)
        {
            resizeDataGrid();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            int validacao = validarCampos();
            if (validacao == 0) { 
                DAL ligacao = new DAL();
                currentUser.username = iptUsername.Text;
                if (!string.IsNullOrEmpty(iptPasswordRepeat.Text))
                {
                    currentUser.Password = Helper.EncryptPass(iptPasswordRepeat.Text);
                }
                currentUser.Admin = iptisAdmin.Text == "True" ? true : false;
                if (this.novo)
                {
                    ligacao.addUser(currentUser);
                    backHome();
                    Helper.launchLogger("Utilizador adicionado", currentUser);
                }
                else
                {
                    ligacao.atualizarUser(currentUser);
                    backHome();
                    Helper.launchLogger("Utilizador editado", currentUser);
                }
            }else if(validacao == 1){
                resetLabels();
                lblErrorsGeral.Visible = true;
                lblErrorsGeral.Text = Helper.listaErros[4];
            }
            else if (validacao == 2)
            {
                resetLabels();
                lblErrorsGeral.Visible = true;
                lblErrorsGeral.Text = Helper.listaErros[5];
            }
            else if (validacao == 3)
            {
                resetLabels();
                lblErrorsGeral.Visible = true;
                lblErrorsGeral.Text = Helper.listaErros[6];
            }
            else if (validacao == 4)
            {
                resetLabels();
                lblErrorsGeral.Visible = true;
                lblErrorsGeral.Text = Helper.listaErros[7];
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            backHome();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string msg = string.Format(Helper.listaErros[10], currentUser.username);
            if (Helper.getUserConfirmation(msg))
            {
                DAL ligacao = new DAL();

                ligacao.deleteUser(currentUser);
                backHome();
            }
        }

        private void iptPassword_TextChanged(object sender, EventArgs e)
        {
            lblErrorsPwd4.ForeColor = Helper.validarComprimento(iptPassword.Text) ? Color.Lime : Color.OrangeRed;
            lblErrorsPwd2.ForeColor = Helper.validarDigitos(iptPassword.Text) ? Color.Lime : Color.OrangeRed;
        }

        private void iptPasswordRepeat_TextChanged(object sender, EventArgs e)
        {
            lblErrorsPwd5.ForeColor = iptPassword.Text == iptPasswordRepeat.Text ? Color.Lime : Color.OrangeRed;
        }

        private void iptPasswordRepeat_Enter(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(iptPassword.Text))
            {
                lblErrorsPwd5.Visible = true;
                lblErrorsPwd5.BringToFront();
            }
        }

        private void iptPassword_Enter(object sender, EventArgs e)
        {
            resetLabels();
            lblErrorsPwd1.Visible = true;
            lblErrorsPwd4.Visible = true;
            lblErrorsPwd3.Visible = true;
            lblErrorsPwd2.Visible = true;
        }

    }
}
