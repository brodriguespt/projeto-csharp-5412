﻿namespace userContacts
{
    partial class frmContacto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.centerPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.colTitulo = new System.Windows.Forms.Panel();
            this.iptTitulo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.colNome = new System.Windows.Forms.Panel();
            this.iptNome = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.colEstado = new System.Windows.Forms.Panel();
            this.iptVisibilidade = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.colMorada = new System.Windows.Forms.Panel();
            this.iptMorada = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.colNIF = new System.Windows.Forms.Panel();
            this.iptNif = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.colEmpresas = new System.Windows.Forms.Panel();
            this.btnDeleteEmpresa = new System.Windows.Forms.Button();
            this.btnAddEmpresa = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.dtgEmpresas = new System.Windows.Forms.DataGridView();
            this.colContactos = new System.Windows.Forms.Panel();
            this.btnDeleteContacto = new System.Windows.Forms.Button();
            this.btnAddContacto = new System.Windows.Forms.Button();
            this.lblcontactos = new System.Windows.Forms.Label();
            this.dtgComunicacao = new System.Windows.Forms.DataGridView();
            this.colDataCriacao = new System.Windows.Forms.Panel();
            this.iptUserCriacao = new System.Windows.Forms.TextBox();
            this.dtDataCriacao = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.colDataModificacao = new System.Windows.Forms.Panel();
            this.iptUserMod = new System.Windows.Forms.TextBox();
            this.dtUserMod = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblErrors = new System.Windows.Forms.Label();
            this.colToolbar = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.empresasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.contactoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.centerPanel.SuspendLayout();
            this.colTitulo.SuspendLayout();
            this.colNome.SuspendLayout();
            this.colEstado.SuspendLayout();
            this.colMorada.SuspendLayout();
            this.colNIF.SuspendLayout();
            this.colEmpresas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgEmpresas)).BeginInit();
            this.colContactos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgComunicacao)).BeginInit();
            this.colDataCriacao.SuspendLayout();
            this.colDataModificacao.SuspendLayout();
            this.panel1.SuspendLayout();
            this.colToolbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.empresasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contactoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // centerPanel
            // 
            this.centerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.centerPanel.Controls.Add(this.colTitulo);
            this.centerPanel.Controls.Add(this.colNome);
            this.centerPanel.Controls.Add(this.colEstado);
            this.centerPanel.Controls.Add(this.colMorada);
            this.centerPanel.Controls.Add(this.colNIF);
            this.centerPanel.Controls.Add(this.colEmpresas);
            this.centerPanel.Controls.Add(this.colContactos);
            this.centerPanel.Controls.Add(this.colDataCriacao);
            this.centerPanel.Controls.Add(this.colDataModificacao);
            this.centerPanel.Controls.Add(this.panel1);
            this.centerPanel.Controls.Add(this.colToolbar);
            this.centerPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.centerPanel.Location = new System.Drawing.Point(114, 68);
            this.centerPanel.Name = "centerPanel";
            this.centerPanel.Size = new System.Drawing.Size(821, 405);
            this.centerPanel.TabIndex = 0;
            // 
            // colTitulo
            // 
            this.colTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.colTitulo.Controls.Add(this.iptTitulo);
            this.colTitulo.Controls.Add(this.label2);
            this.colTitulo.Location = new System.Drawing.Point(0, 0);
            this.colTitulo.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.colTitulo.Name = "colTitulo";
            this.colTitulo.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colTitulo.Size = new System.Drawing.Size(93, 50);
            this.colTitulo.TabIndex = 2;
            // 
            // iptTitulo
            // 
            this.iptTitulo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.iptTitulo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.iptTitulo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iptTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iptTitulo.FormattingEnabled = true;
            this.iptTitulo.Items.AddRange(new object[] {
            "Senhor",
            "Senhora",
            "Outro"});
            this.iptTitulo.Location = new System.Drawing.Point(2, 22);
            this.iptTitulo.Name = "iptTitulo";
            this.iptTitulo.Size = new System.Drawing.Size(89, 28);
            this.iptTitulo.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(2, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(2);
            this.label2.Size = new System.Drawing.Size(47, 21);
            this.label2.TabIndex = 0;
            this.label2.Text = "Titulo";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colNome
            // 
            this.colNome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.colNome.Controls.Add(this.iptNome);
            this.colNome.Controls.Add(this.label1);
            this.colNome.Location = new System.Drawing.Point(93, 0);
            this.colNome.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.colNome.Name = "colNome";
            this.colNome.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colNome.Size = new System.Drawing.Size(93, 50);
            this.colNome.TabIndex = 3;
            // 
            // iptNome
            // 
            this.iptNome.BackColor = System.Drawing.Color.White;
            this.iptNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iptNome.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.iptNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iptNome.Location = new System.Drawing.Point(2, 21);
            this.iptNome.Name = "iptNome";
            this.iptNome.Size = new System.Drawing.Size(89, 29);
            this.iptNome.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(2, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(2);
            this.label1.Size = new System.Drawing.Size(49, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colEstado
            // 
            this.colEstado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.colEstado.Controls.Add(this.iptVisibilidade);
            this.colEstado.Controls.Add(this.label4);
            this.colEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colEstado.Location = new System.Drawing.Point(186, 0);
            this.colEstado.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.colEstado.Name = "colEstado";
            this.colEstado.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colEstado.Size = new System.Drawing.Size(155, 50);
            this.colEstado.TabIndex = 3;
            // 
            // iptVisibilidade
            // 
            this.iptVisibilidade.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.iptVisibilidade.DropDownHeight = 100;
            this.iptVisibilidade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.iptVisibilidade.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iptVisibilidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iptVisibilidade.FormattingEnabled = true;
            this.iptVisibilidade.IntegralHeight = false;
            this.iptVisibilidade.ItemHeight = 20;
            this.iptVisibilidade.Items.AddRange(new object[] {
            "Publico",
            "Privado"});
            this.iptVisibilidade.Location = new System.Drawing.Point(2, 22);
            this.iptVisibilidade.Name = "iptVisibilidade";
            this.iptVisibilidade.Size = new System.Drawing.Size(151, 28);
            this.iptVisibilidade.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(2, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(2);
            this.label4.Size = new System.Drawing.Size(83, 21);
            this.label4.TabIndex = 0;
            this.label4.Text = "Visibilidade";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colMorada
            // 
            this.colMorada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.colMorada.Controls.Add(this.iptMorada);
            this.colMorada.Controls.Add(this.label3);
            this.colMorada.Location = new System.Drawing.Point(341, 0);
            this.colMorada.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.colMorada.Name = "colMorada";
            this.colMorada.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colMorada.Size = new System.Drawing.Size(144, 50);
            this.colMorada.TabIndex = 4;
            // 
            // iptMorada
            // 
            this.iptMorada.BackColor = System.Drawing.Color.White;
            this.iptMorada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iptMorada.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.iptMorada.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iptMorada.Location = new System.Drawing.Point(2, 21);
            this.iptMorada.Name = "iptMorada";
            this.iptMorada.Size = new System.Drawing.Size(140, 29);
            this.iptMorada.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(2, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(2);
            this.label3.Size = new System.Drawing.Size(60, 21);
            this.label3.TabIndex = 0;
            this.label3.Text = "Morada";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colNIF
            // 
            this.colNIF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.colNIF.Controls.Add(this.iptNif);
            this.colNIF.Controls.Add(this.label5);
            this.colNIF.Location = new System.Drawing.Point(485, 0);
            this.colNIF.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.colNIF.Name = "colNIF";
            this.colNIF.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colNIF.Size = new System.Drawing.Size(161, 50);
            this.colNIF.TabIndex = 5;
            // 
            // iptNif
            // 
            this.iptNif.BackColor = System.Drawing.Color.White;
            this.iptNif.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iptNif.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.iptNif.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iptNif.Location = new System.Drawing.Point(2, 21);
            this.iptNif.Name = "iptNif";
            this.iptNif.Size = new System.Drawing.Size(157, 29);
            this.iptNif.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(2, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(2);
            this.label5.Size = new System.Drawing.Size(33, 21);
            this.label5.TabIndex = 0;
            this.label5.Text = "NIF";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colEmpresas
            // 
            this.colEmpresas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.colEmpresas.Controls.Add(this.btnDeleteEmpresa);
            this.colEmpresas.Controls.Add(this.btnAddEmpresa);
            this.colEmpresas.Controls.Add(this.label8);
            this.colEmpresas.Controls.Add(this.dtgEmpresas);
            this.colEmpresas.Location = new System.Drawing.Point(0, 60);
            this.colEmpresas.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.colEmpresas.Name = "colEmpresas";
            this.colEmpresas.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colEmpresas.Size = new System.Drawing.Size(212, 150);
            this.colEmpresas.TabIndex = 8;
            // 
            // btnDeleteEmpresa
            // 
            this.btnDeleteEmpresa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(80)))), ((int)(((byte)(97)))));
            this.btnDeleteEmpresa.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDeleteEmpresa.FlatAppearance.BorderSize = 0;
            this.btnDeleteEmpresa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteEmpresa.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnDeleteEmpresa.Location = new System.Drawing.Point(154, 0);
            this.btnDeleteEmpresa.Margin = new System.Windows.Forms.Padding(0);
            this.btnDeleteEmpresa.Name = "btnDeleteEmpresa";
            this.btnDeleteEmpresa.Size = new System.Drawing.Size(28, 30);
            this.btnDeleteEmpresa.TabIndex = 7;
            this.btnDeleteEmpresa.Text = "-";
            this.btnDeleteEmpresa.UseVisualStyleBackColor = false;
            this.btnDeleteEmpresa.Click += new System.EventHandler(this.btnDeleteEmpresa_Click);
            // 
            // btnAddEmpresa
            // 
            this.btnAddEmpresa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(128)))), ((int)(((byte)(188)))));
            this.btnAddEmpresa.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAddEmpresa.FlatAppearance.BorderSize = 0;
            this.btnAddEmpresa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddEmpresa.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnAddEmpresa.Location = new System.Drawing.Point(182, 0);
            this.btnAddEmpresa.Margin = new System.Windows.Forms.Padding(0);
            this.btnAddEmpresa.Name = "btnAddEmpresa";
            this.btnAddEmpresa.Size = new System.Drawing.Size(28, 30);
            this.btnAddEmpresa.TabIndex = 6;
            this.btnAddEmpresa.Text = "+";
            this.btnAddEmpresa.UseVisualStyleBackColor = false;
            this.btnAddEmpresa.Click += new System.EventHandler(this.btnAddEmpresa_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(2, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.label8.Size = new System.Drawing.Size(75, 24);
            this.label8.TabIndex = 0;
            this.label8.Text = "Empresas";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtgEmpresas
            // 
            this.dtgEmpresas.AllowUserToResizeRows = false;
            this.dtgEmpresas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgEmpresas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(255)))));
            this.dtgEmpresas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtgEmpresas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgEmpresas.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dtgEmpresas.Location = new System.Drawing.Point(2, 30);
            this.dtgEmpresas.MultiSelect = false;
            this.dtgEmpresas.Name = "dtgEmpresas";
            this.dtgEmpresas.RowHeadersVisible = false;
            this.dtgEmpresas.RowTemplate.Height = 30;
            this.dtgEmpresas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dtgEmpresas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgEmpresas.Size = new System.Drawing.Size(208, 120);
            this.dtgEmpresas.TabIndex = 1;
            // 
            // colContactos
            // 
            this.colContactos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.colContactos.Controls.Add(this.btnDeleteContacto);
            this.colContactos.Controls.Add(this.btnAddContacto);
            this.colContactos.Controls.Add(this.lblcontactos);
            this.colContactos.Controls.Add(this.dtgComunicacao);
            this.colContactos.Location = new System.Drawing.Point(212, 60);
            this.colContactos.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.colContactos.Name = "colContactos";
            this.colContactos.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colContactos.Size = new System.Drawing.Size(212, 150);
            this.colContactos.TabIndex = 9;
            // 
            // btnDeleteContacto
            // 
            this.btnDeleteContacto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(80)))), ((int)(((byte)(97)))));
            this.btnDeleteContacto.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDeleteContacto.FlatAppearance.BorderSize = 0;
            this.btnDeleteContacto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteContacto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteContacto.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnDeleteContacto.Location = new System.Drawing.Point(154, 0);
            this.btnDeleteContacto.Margin = new System.Windows.Forms.Padding(0);
            this.btnDeleteContacto.Name = "btnDeleteContacto";
            this.btnDeleteContacto.Size = new System.Drawing.Size(28, 30);
            this.btnDeleteContacto.TabIndex = 7;
            this.btnDeleteContacto.Text = "-";
            this.btnDeleteContacto.UseVisualStyleBackColor = false;
            this.btnDeleteContacto.Click += new System.EventHandler(this.BtnDeleteContacto_Click);
            // 
            // btnAddContacto
            // 
            this.btnAddContacto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(128)))), ((int)(((byte)(188)))));
            this.btnAddContacto.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAddContacto.FlatAppearance.BorderSize = 0;
            this.btnAddContacto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddContacto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddContacto.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnAddContacto.Location = new System.Drawing.Point(182, 0);
            this.btnAddContacto.Margin = new System.Windows.Forms.Padding(0);
            this.btnAddContacto.Name = "btnAddContacto";
            this.btnAddContacto.Size = new System.Drawing.Size(28, 30);
            this.btnAddContacto.TabIndex = 6;
            this.btnAddContacto.Text = "+";
            this.btnAddContacto.UseVisualStyleBackColor = false;
            this.btnAddContacto.Click += new System.EventHandler(this.BtnAddContacto_Click);
            // 
            // lblcontactos
            // 
            this.lblcontactos.AutoSize = true;
            this.lblcontactos.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblcontactos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcontactos.ForeColor = System.Drawing.Color.White;
            this.lblcontactos.Location = new System.Drawing.Point(2, 0);
            this.lblcontactos.Margin = new System.Windows.Forms.Padding(0);
            this.lblcontactos.Name = "lblcontactos";
            this.lblcontactos.Padding = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.lblcontactos.Size = new System.Drawing.Size(75, 24);
            this.lblcontactos.TabIndex = 0;
            this.lblcontactos.Text = "Contactos";
            this.lblcontactos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtgComunicacao
            // 
            this.dtgComunicacao.AllowUserToResizeRows = false;
            this.dtgComunicacao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgComunicacao.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(255)))));
            this.dtgComunicacao.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtgComunicacao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgComunicacao.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dtgComunicacao.Location = new System.Drawing.Point(2, 30);
            this.dtgComunicacao.MultiSelect = false;
            this.dtgComunicacao.Name = "dtgComunicacao";
            this.dtgComunicacao.RowHeadersVisible = false;
            this.dtgComunicacao.RowTemplate.Height = 30;
            this.dtgComunicacao.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dtgComunicacao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgComunicacao.Size = new System.Drawing.Size(208, 120);
            this.dtgComunicacao.TabIndex = 1;
            this.dtgComunicacao.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgComunicacao_CellEndEdit);
            // 
            // colDataCriacao
            // 
            this.colDataCriacao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.colDataCriacao.Controls.Add(this.iptUserCriacao);
            this.colDataCriacao.Controls.Add(this.dtDataCriacao);
            this.colDataCriacao.Controls.Add(this.label6);
            this.colDataCriacao.Location = new System.Drawing.Point(424, 60);
            this.colDataCriacao.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.colDataCriacao.Name = "colDataCriacao";
            this.colDataCriacao.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colDataCriacao.Size = new System.Drawing.Size(212, 80);
            this.colDataCriacao.TabIndex = 6;
            // 
            // iptUserCriacao
            // 
            this.iptUserCriacao.BackColor = System.Drawing.Color.White;
            this.iptUserCriacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iptUserCriacao.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.iptUserCriacao.Enabled = false;
            this.iptUserCriacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iptUserCriacao.Location = new System.Drawing.Point(2, 22);
            this.iptUserCriacao.Margin = new System.Windows.Forms.Padding(0);
            this.iptUserCriacao.Name = "iptUserCriacao";
            this.iptUserCriacao.Size = new System.Drawing.Size(208, 29);
            this.iptUserCriacao.TabIndex = 3;
            // 
            // dtDataCriacao
            // 
            this.dtDataCriacao.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtDataCriacao.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dtDataCriacao.Enabled = false;
            this.dtDataCriacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDataCriacao.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtDataCriacao.Location = new System.Drawing.Point(2, 51);
            this.dtDataCriacao.Margin = new System.Windows.Forms.Padding(0);
            this.dtDataCriacao.Name = "dtDataCriacao";
            this.dtDataCriacao.Size = new System.Drawing.Size(208, 29);
            this.dtDataCriacao.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(2, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(2);
            this.label6.Size = new System.Drawing.Size(165, 21);
            this.label6.TabIndex = 0;
            this.label6.Text = "Utilizador / Data Criação";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colDataModificacao
            // 
            this.colDataModificacao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.colDataModificacao.Controls.Add(this.iptUserMod);
            this.colDataModificacao.Controls.Add(this.dtUserMod);
            this.colDataModificacao.Controls.Add(this.label7);
            this.colDataModificacao.Location = new System.Drawing.Point(0, 220);
            this.colDataModificacao.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.colDataModificacao.Name = "colDataModificacao";
            this.colDataModificacao.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colDataModificacao.Size = new System.Drawing.Size(212, 80);
            this.colDataModificacao.TabIndex = 7;
            // 
            // iptUserMod
            // 
            this.iptUserMod.BackColor = System.Drawing.Color.White;
            this.iptUserMod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iptUserMod.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.iptUserMod.Enabled = false;
            this.iptUserMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iptUserMod.Location = new System.Drawing.Point(2, 22);
            this.iptUserMod.Margin = new System.Windows.Forms.Padding(0);
            this.iptUserMod.Name = "iptUserMod";
            this.iptUserMod.Size = new System.Drawing.Size(208, 29);
            this.iptUserMod.TabIndex = 3;
            // 
            // dtUserMod
            // 
            this.dtUserMod.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtUserMod.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dtUserMod.Enabled = false;
            this.dtUserMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtUserMod.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtUserMod.Location = new System.Drawing.Point(2, 51);
            this.dtUserMod.Margin = new System.Windows.Forms.Padding(0);
            this.dtUserMod.Name = "dtUserMod";
            this.dtUserMod.Size = new System.Drawing.Size(208, 29);
            this.dtUserMod.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(2, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(2);
            this.label7.Size = new System.Drawing.Size(192, 21);
            this.label7.TabIndex = 0;
            this.label7.Text = "Utilizador / Data Modificação";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblErrors);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 310);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.panel1.Size = new System.Drawing.Size(696, 24);
            this.panel1.TabIndex = 10;
            // 
            // lblErrors
            // 
            this.lblErrors.AutoSize = true;
            this.lblErrors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblErrors.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrors.ForeColor = System.Drawing.Color.White;
            this.lblErrors.Location = new System.Drawing.Point(2, 0);
            this.lblErrors.Name = "lblErrors";
            this.lblErrors.Size = new System.Drawing.Size(93, 17);
            this.lblErrors.TabIndex = 5;
            this.lblErrors.Text = "Erros go aqui";
            this.lblErrors.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colToolbar
            // 
            this.colToolbar.Controls.Add(this.btnBack);
            this.colToolbar.Controls.Add(this.btnDelete);
            this.colToolbar.Controls.Add(this.btnSave);
            this.colToolbar.Location = new System.Drawing.Point(0, 334);
            this.colToolbar.Margin = new System.Windows.Forms.Padding(0);
            this.colToolbar.Name = "colToolbar";
            this.colToolbar.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colToolbar.Size = new System.Drawing.Size(696, 34);
            this.colToolbar.TabIndex = 5;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(80)))), ((int)(((byte)(97)))));
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnBack.Location = new System.Drawing.Point(102, 0);
            this.btnBack.Margin = new System.Windows.Forms.Padding(0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(100, 34);
            this.btnBack.TabIndex = 6;
            this.btnBack.Text = "Voltar";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDelete.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnDelete.Location = new System.Drawing.Point(2, 0);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 34);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "Apagar";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(128)))), ((int)(((byte)(188)))));
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnSave.Location = new System.Drawing.Point(594, 0);
            this.btnSave.Margin = new System.Windows.Forms.Padding(0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 34);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Guardar";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // frmContacto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.centerPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmContacto";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Panel";
            this.SizeChanged += new System.EventHandler(this.FrmMeusContactos_ResizeEnd);
            this.centerPanel.ResumeLayout(false);
            this.colTitulo.ResumeLayout(false);
            this.colTitulo.PerformLayout();
            this.colNome.ResumeLayout(false);
            this.colNome.PerformLayout();
            this.colEstado.ResumeLayout(false);
            this.colEstado.PerformLayout();
            this.colMorada.ResumeLayout(false);
            this.colMorada.PerformLayout();
            this.colNIF.ResumeLayout(false);
            this.colNIF.PerformLayout();
            this.colEmpresas.ResumeLayout(false);
            this.colEmpresas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgEmpresas)).EndInit();
            this.colContactos.ResumeLayout(false);
            this.colContactos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgComunicacao)).EndInit();
            this.colDataCriacao.ResumeLayout(false);
            this.colDataCriacao.PerformLayout();
            this.colDataModificacao.ResumeLayout(false);
            this.colDataModificacao.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.colToolbar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.empresasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contactoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel centerPanel;
        private System.Windows.Forms.Panel colTitulo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox iptTitulo;
        private System.Windows.Forms.Panel colNome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox iptNome;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel colMorada;
        private System.Windows.Forms.TextBox iptMorada;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel colToolbar;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Panel colEstado;
        private System.Windows.Forms.ComboBox iptVisibilidade;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel colNIF;
        private System.Windows.Forms.TextBox iptNif;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel colDataCriacao;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox iptUserCriacao;
        private System.Windows.Forms.DateTimePicker dtDataCriacao;
        private System.Windows.Forms.Panel colDataModificacao;
        private System.Windows.Forms.TextBox iptUserMod;
        private System.Windows.Forms.DateTimePicker dtUserMod;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel colEmpresas;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.BindingSource empresasBindingSource;
        private System.Windows.Forms.BindingSource contactoBindingSource;
        private System.Windows.Forms.DataGridView dtgEmpresas;
        private System.Windows.Forms.Button btnAddEmpresa;
        private System.Windows.Forms.Button btnDeleteEmpresa;
        private System.Windows.Forms.Panel colContactos;
        private System.Windows.Forms.Button btnDeleteContacto;
        private System.Windows.Forms.Button btnAddContacto;
        private System.Windows.Forms.Label lblcontactos;
        private System.Windows.Forms.DataGridView dtgComunicacao;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblErrors;
    }
}