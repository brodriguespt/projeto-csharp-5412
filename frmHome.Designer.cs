﻿namespace userContacts
{
    partial class frmHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataContactosPublicos = new System.Windows.Forms.DataGridView();
            this.lblTitle = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataContactosPublicos)).BeginInit();
            this.SuspendLayout();
            // 
            // dataContactosPublicos
            // 
            this.dataContactosPublicos.AllowUserToAddRows = false;
            this.dataContactosPublicos.AllowUserToDeleteRows = false;
            this.dataContactosPublicos.AllowUserToResizeRows = false;
            this.dataContactosPublicos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataContactosPublicos.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(255)))));
            this.dataContactosPublicos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataContactosPublicos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataContactosPublicos.Location = new System.Drawing.Point(323, 187);
            this.dataContactosPublicos.Name = "dataContactosPublicos";
            this.dataContactosPublicos.ReadOnly = true;
            this.dataContactosPublicos.RowHeadersVisible = false;
            this.dataContactosPublicos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataContactosPublicos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataContactosPublicos.Size = new System.Drawing.Size(517, 150);
            this.dataContactosPublicos.TabIndex = 0;
            this.dataContactosPublicos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataContactos_CellContentDoubleClick);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(100, 20);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(227, 20);
            this.lblTitle.TabIndex = 4;
            this.lblTitle.Text = "Doubleclick para abrir contacto";
            // 
            // frmHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.dataContactosPublicos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmHome";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Panel";
            this.Resize += new System.EventHandler(this.FrmMeusContactos_ResizeEnd);
            ((System.ComponentModel.ISupportInitialize)(this.dataContactosPublicos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataContactosPublicos;
        private System.Windows.Forms.Label lblTitle;
    }
}