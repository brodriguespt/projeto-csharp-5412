## Projeto CSharp 5412

Pretende-se criar uma aplicação de contactos que permita fazer o seguinte:
Login para múltiplos utilizadores da aplicação;
Cada utilizador terá a sua lista de contactos;
Cada utilizador pode gerir os seus contactos, adicionando, alterando ou apagando da lista de contactos;
Um utilizador armazena a informação sobre o seu id, nome e palavra-passe;

Os contactos deverão armazenar a seguinte informação:
* Id;
* Nome;
* Título;
* Morada (com código postal e localidade);
* NIF;
* Empresas onde trabalha (pode ser mais do que uma);
* Formas de Comunicação (pode ter várias e de vários tipos iguais ou diferentes. Ex: um contacto pode ter vários e-mails, vários telefones, etc…);
* Público ou privado (no caso de ser público, está disponível para todos os utilizadores);
* Utilizador que criou o contacto;
* Data de criação do contacto;
* Utilizador que fez a última modificação do contacto;
* Data da última modificação do contacto;

Cada contacto armazenado na aplicação apenas pode ser acedido e/ou modificado pelo utilizador que o criou,
exceção feita aos contactos públicos que podem ser alterados por qualquer utilizador;
Listar contactos privados, públicos, por utilizador, com e sem filtragem por qualquer tipo de campo;
A aplicação deve ser feita em Windows Forms, suportada por uma base de dados Microsoft Access.