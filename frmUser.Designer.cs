﻿namespace userContacts
{
    partial class frmUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.centerPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.colUser = new System.Windows.Forms.Panel();
            this.iptUsername = new System.Windows.Forms.TextBox();
            this.ErrorlblUsername = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.colAdmin = new System.Windows.Forms.Panel();
            this.iptisAdmin = new System.Windows.Forms.ComboBox();
            this.ErrorlblAdmin = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.colPwd1 = new System.Windows.Forms.Panel();
            this.ErrorlblPassword = new System.Windows.Forms.Label();
            this.iptPassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.colPwd2 = new System.Windows.Forms.Panel();
            this.ErrorlblRepeatPassword = new System.Windows.Forms.Label();
            this.iptPasswordRepeat = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblErrorsGeral = new System.Windows.Forms.Label();
            this.lblErrorsPwd5 = new System.Windows.Forms.Label();
            this.lblErrorsPwd4 = new System.Windows.Forms.Label();
            this.lblErrorsPwd2 = new System.Windows.Forms.Label();
            this.lblErrorsPwd3 = new System.Windows.Forms.Label();
            this.lblErrorsPwd1 = new System.Windows.Forms.Label();
            this.colToolbar = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.empresasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.contactoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.centerPanel.SuspendLayout();
            this.colUser.SuspendLayout();
            this.colAdmin.SuspendLayout();
            this.colPwd1.SuspendLayout();
            this.colPwd2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.colToolbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.empresasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contactoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // centerPanel
            // 
            this.centerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.centerPanel.Controls.Add(this.colUser);
            this.centerPanel.Controls.Add(this.colAdmin);
            this.centerPanel.Controls.Add(this.colPwd1);
            this.centerPanel.Controls.Add(this.colPwd2);
            this.centerPanel.Controls.Add(this.panel1);
            this.centerPanel.Controls.Add(this.colToolbar);
            this.centerPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.centerPanel.Location = new System.Drawing.Point(114, 68);
            this.centerPanel.Name = "centerPanel";
            this.centerPanel.Size = new System.Drawing.Size(821, 405);
            this.centerPanel.TabIndex = 0;
            // 
            // colUser
            // 
            this.colUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.colUser.Controls.Add(this.iptUsername);
            this.colUser.Controls.Add(this.ErrorlblUsername);
            this.colUser.Controls.Add(this.label1);
            this.colUser.Location = new System.Drawing.Point(0, 0);
            this.colUser.Margin = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.colUser.Name = "colUser";
            this.colUser.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colUser.Size = new System.Drawing.Size(317, 50);
            this.colUser.TabIndex = 3;
            // 
            // iptUsername
            // 
            this.iptUsername.BackColor = System.Drawing.Color.White;
            this.iptUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iptUsername.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.iptUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iptUsername.Location = new System.Drawing.Point(2, 21);
            this.iptUsername.Name = "iptUsername";
            this.iptUsername.Size = new System.Drawing.Size(313, 29);
            this.iptUsername.TabIndex = 3;
            // 
            // ErrorlblUsername
            // 
            this.ErrorlblUsername.AutoSize = true;
            this.ErrorlblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorlblUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ErrorlblUsername.Location = new System.Drawing.Point(79, 0);
            this.ErrorlblUsername.Margin = new System.Windows.Forms.Padding(0);
            this.ErrorlblUsername.Name = "ErrorlblUsername";
            this.ErrorlblUsername.Size = new System.Drawing.Size(13, 17);
            this.ErrorlblUsername.TabIndex = 4;
            this.ErrorlblUsername.Text = "*";
            this.ErrorlblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ErrorlblUsername.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(2, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(2);
            this.label1.Size = new System.Drawing.Size(77, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colAdmin
            // 
            this.colAdmin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.colAdmin.Controls.Add(this.iptisAdmin);
            this.colAdmin.Controls.Add(this.ErrorlblAdmin);
            this.colAdmin.Controls.Add(this.label4);
            this.colAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colAdmin.Location = new System.Drawing.Point(317, 0);
            this.colAdmin.Margin = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.colAdmin.Name = "colAdmin";
            this.colAdmin.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colAdmin.Size = new System.Drawing.Size(93, 50);
            this.colAdmin.TabIndex = 3;
            // 
            // iptisAdmin
            // 
            this.iptisAdmin.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.iptisAdmin.DropDownHeight = 100;
            this.iptisAdmin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.iptisAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iptisAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iptisAdmin.FormattingEnabled = true;
            this.iptisAdmin.IntegralHeight = false;
            this.iptisAdmin.ItemHeight = 20;
            this.iptisAdmin.Items.AddRange(new object[] {
            "True",
            "False"});
            this.iptisAdmin.Location = new System.Drawing.Point(2, 22);
            this.iptisAdmin.Name = "iptisAdmin";
            this.iptisAdmin.Size = new System.Drawing.Size(89, 28);
            this.iptisAdmin.TabIndex = 2;
            // 
            // ErrorlblAdmin
            // 
            this.ErrorlblAdmin.AutoSize = true;
            this.ErrorlblAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorlblAdmin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ErrorlblAdmin.Location = new System.Drawing.Point(53, 0);
            this.ErrorlblAdmin.Margin = new System.Windows.Forms.Padding(0);
            this.ErrorlblAdmin.Name = "ErrorlblAdmin";
            this.ErrorlblAdmin.Padding = new System.Windows.Forms.Padding(2);
            this.ErrorlblAdmin.Size = new System.Drawing.Size(17, 21);
            this.ErrorlblAdmin.TabIndex = 5;
            this.ErrorlblAdmin.Text = "*";
            this.ErrorlblAdmin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ErrorlblAdmin.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(2, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(2);
            this.label4.Size = new System.Drawing.Size(51, 21);
            this.label4.TabIndex = 0;
            this.label4.Text = "Admin";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colPwd1
            // 
            this.colPwd1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.colPwd1.Controls.Add(this.ErrorlblPassword);
            this.colPwd1.Controls.Add(this.iptPassword);
            this.colPwd1.Controls.Add(this.label3);
            this.colPwd1.Location = new System.Drawing.Point(410, 0);
            this.colPwd1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.colPwd1.Name = "colPwd1";
            this.colPwd1.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colPwd1.Size = new System.Drawing.Size(93, 50);
            this.colPwd1.TabIndex = 4;
            // 
            // ErrorlblPassword
            // 
            this.ErrorlblPassword.AutoSize = true;
            this.ErrorlblPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorlblPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ErrorlblPassword.Location = new System.Drawing.Point(74, 0);
            this.ErrorlblPassword.Margin = new System.Windows.Forms.Padding(0);
            this.ErrorlblPassword.Name = "ErrorlblPassword";
            this.ErrorlblPassword.Padding = new System.Windows.Forms.Padding(2);
            this.ErrorlblPassword.Size = new System.Drawing.Size(17, 21);
            this.ErrorlblPassword.TabIndex = 6;
            this.ErrorlblPassword.Text = "*";
            this.ErrorlblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ErrorlblPassword.Visible = false;
            // 
            // iptPassword
            // 
            this.iptPassword.BackColor = System.Drawing.Color.White;
            this.iptPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iptPassword.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.iptPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iptPassword.Location = new System.Drawing.Point(2, 21);
            this.iptPassword.Name = "iptPassword";
            this.iptPassword.PasswordChar = '·';
            this.iptPassword.Size = new System.Drawing.Size(89, 29);
            this.iptPassword.TabIndex = 3;
            this.iptPassword.TextChanged += new System.EventHandler(this.iptPassword_TextChanged);
            this.iptPassword.Enter += new System.EventHandler(this.iptPassword_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(2, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(2);
            this.label3.Size = new System.Drawing.Size(73, 21);
            this.label3.TabIndex = 0;
            this.label3.Text = "Password";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colPwd2
            // 
            this.colPwd2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.colPwd2.Controls.Add(this.ErrorlblRepeatPassword);
            this.colPwd2.Controls.Add(this.iptPasswordRepeat);
            this.colPwd2.Controls.Add(this.label2);
            this.colPwd2.Location = new System.Drawing.Point(503, 0);
            this.colPwd2.Margin = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.colPwd2.Name = "colPwd2";
            this.colPwd2.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colPwd2.Size = new System.Drawing.Size(252, 50);
            this.colPwd2.TabIndex = 5;
            // 
            // ErrorlblRepeatPassword
            // 
            this.ErrorlblRepeatPassword.AutoSize = true;
            this.ErrorlblRepeatPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorlblRepeatPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ErrorlblRepeatPassword.Location = new System.Drawing.Point(125, 0);
            this.ErrorlblRepeatPassword.Margin = new System.Windows.Forms.Padding(0);
            this.ErrorlblRepeatPassword.Name = "ErrorlblRepeatPassword";
            this.ErrorlblRepeatPassword.Padding = new System.Windows.Forms.Padding(2);
            this.ErrorlblRepeatPassword.Size = new System.Drawing.Size(17, 21);
            this.ErrorlblRepeatPassword.TabIndex = 7;
            this.ErrorlblRepeatPassword.Text = "*";
            this.ErrorlblRepeatPassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ErrorlblRepeatPassword.Visible = false;
            // 
            // iptPasswordRepeat
            // 
            this.iptPasswordRepeat.BackColor = System.Drawing.Color.White;
            this.iptPasswordRepeat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iptPasswordRepeat.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.iptPasswordRepeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iptPasswordRepeat.Location = new System.Drawing.Point(2, 21);
            this.iptPasswordRepeat.Name = "iptPasswordRepeat";
            this.iptPasswordRepeat.PasswordChar = '·';
            this.iptPasswordRepeat.Size = new System.Drawing.Size(248, 29);
            this.iptPasswordRepeat.TabIndex = 3;
            this.iptPasswordRepeat.TextChanged += new System.EventHandler(this.iptPasswordRepeat_TextChanged);
            this.iptPasswordRepeat.Enter += new System.EventHandler(this.iptPasswordRepeat_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(2, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(2);
            this.label2.Size = new System.Drawing.Size(123, 21);
            this.label2.TabIndex = 0;
            this.label2.Text = "Repetir Password";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblErrorsGeral);
            this.panel1.Controls.Add(this.lblErrorsPwd5);
            this.panel1.Controls.Add(this.lblErrorsPwd4);
            this.panel1.Controls.Add(this.lblErrorsPwd2);
            this.panel1.Controls.Add(this.lblErrorsPwd3);
            this.panel1.Controls.Add(this.lblErrorsPwd1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 70);
            this.panel1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.panel1.Size = new System.Drawing.Size(696, 24);
            this.panel1.TabIndex = 11;
            // 
            // lblErrorsGeral
            // 
            this.lblErrorsGeral.AutoSize = true;
            this.lblErrorsGeral.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblErrorsGeral.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorsGeral.ForeColor = System.Drawing.Color.White;
            this.lblErrorsGeral.Location = new System.Drawing.Point(588, 0);
            this.lblErrorsGeral.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrorsGeral.Name = "lblErrorsGeral";
            this.lblErrorsGeral.Size = new System.Drawing.Size(129, 17);
            this.lblErrorsGeral.TabIndex = 6;
            this.lblErrorsGeral.Text = "Username inválido.";
            this.lblErrorsGeral.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblErrorsGeral.Visible = false;
            // 
            // lblErrorsPwd5
            // 
            this.lblErrorsPwd5.AutoSize = true;
            this.lblErrorsPwd5.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblErrorsPwd5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorsPwd5.ForeColor = System.Drawing.Color.White;
            this.lblErrorsPwd5.Location = new System.Drawing.Point(425, 0);
            this.lblErrorsPwd5.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrorsPwd5.Name = "lblErrorsPwd5";
            this.lblErrorsPwd5.Size = new System.Drawing.Size(163, 17);
            this.lblErrorsPwd5.TabIndex = 5;
            this.lblErrorsPwd5.Text = "Password repetida igual.";
            this.lblErrorsPwd5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblErrorsPwd5.Visible = false;
            // 
            // lblErrorsPwd4
            // 
            this.lblErrorsPwd4.AutoSize = true;
            this.lblErrorsPwd4.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblErrorsPwd4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorsPwd4.ForeColor = System.Drawing.Color.White;
            this.lblErrorsPwd4.Location = new System.Drawing.Point(317, 0);
            this.lblErrorsPwd4.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrorsPwd4.Name = "lblErrorsPwd4";
            this.lblErrorsPwd4.Size = new System.Drawing.Size(108, 17);
            this.lblErrorsPwd4.TabIndex = 2;
            this.lblErrorsPwd4.Text = "incluir numeros.";
            this.lblErrorsPwd4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblErrorsPwd4.Visible = false;
            // 
            // lblErrorsPwd2
            // 
            this.lblErrorsPwd2.AutoSize = true;
            this.lblErrorsPwd2.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblErrorsPwd2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorsPwd2.ForeColor = System.Drawing.Color.White;
            this.lblErrorsPwd2.Location = new System.Drawing.Point(230, 0);
            this.lblErrorsPwd2.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrorsPwd2.Name = "lblErrorsPwd2";
            this.lblErrorsPwd2.Size = new System.Drawing.Size(87, 17);
            this.lblErrorsPwd2.TabIndex = 4;
            this.lblErrorsPwd2.Text = "6 caracteres";
            this.lblErrorsPwd2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblErrorsPwd2.Visible = false;
            // 
            // lblErrorsPwd3
            // 
            this.lblErrorsPwd3.AutoSize = true;
            this.lblErrorsPwd3.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblErrorsPwd3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorsPwd3.ForeColor = System.Drawing.Color.White;
            this.lblErrorsPwd3.Location = new System.Drawing.Point(214, 0);
            this.lblErrorsPwd3.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrorsPwd3.Name = "lblErrorsPwd3";
            this.lblErrorsPwd3.Size = new System.Drawing.Size(16, 17);
            this.lblErrorsPwd3.TabIndex = 3;
            this.lblErrorsPwd3.Text = "e";
            this.lblErrorsPwd3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblErrorsPwd3.Visible = false;
            // 
            // lblErrorsPwd1
            // 
            this.lblErrorsPwd1.AutoSize = true;
            this.lblErrorsPwd1.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblErrorsPwd1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorsPwd1.ForeColor = System.Drawing.Color.White;
            this.lblErrorsPwd1.Location = new System.Drawing.Point(2, 0);
            this.lblErrorsPwd1.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrorsPwd1.Name = "lblErrorsPwd1";
            this.lblErrorsPwd1.Size = new System.Drawing.Size(212, 17);
            this.lblErrorsPwd1.TabIndex = 1;
            this.lblErrorsPwd1.Text = "Password precisa ter no minimo ";
            this.lblErrorsPwd1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblErrorsPwd1.Visible = false;
            // 
            // colToolbar
            // 
            this.colToolbar.Controls.Add(this.btnBack);
            this.colToolbar.Controls.Add(this.btnDelete);
            this.colToolbar.Controls.Add(this.btnSave);
            this.colToolbar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.colToolbar.Location = new System.Drawing.Point(0, 114);
            this.colToolbar.Margin = new System.Windows.Forms.Padding(0);
            this.colToolbar.Name = "colToolbar";
            this.colToolbar.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.colToolbar.Size = new System.Drawing.Size(696, 34);
            this.colToolbar.TabIndex = 5;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(80)))), ((int)(((byte)(97)))));
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnBack.Location = new System.Drawing.Point(102, 0);
            this.btnBack.Margin = new System.Windows.Forms.Padding(0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(100, 34);
            this.btnBack.TabIndex = 6;
            this.btnBack.Text = "Voltar";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDelete.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnDelete.Location = new System.Drawing.Point(2, 0);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 34);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "Apagar";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(128)))), ((int)(((byte)(188)))));
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnSave.Location = new System.Drawing.Point(594, 0);
            this.btnSave.Margin = new System.Windows.Forms.Padding(0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 34);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Guardar";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // frmUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.centerPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUser";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Panel";
            this.SizeChanged += new System.EventHandler(this.FrmMeusContactos_ResizeEnd);
            this.centerPanel.ResumeLayout(false);
            this.colUser.ResumeLayout(false);
            this.colUser.PerformLayout();
            this.colAdmin.ResumeLayout(false);
            this.colAdmin.PerformLayout();
            this.colPwd1.ResumeLayout(false);
            this.colPwd1.PerformLayout();
            this.colPwd2.ResumeLayout(false);
            this.colPwd2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.colToolbar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.empresasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contactoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel centerPanel;
        private System.Windows.Forms.Panel colUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox iptUsername;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel colPwd1;
        private System.Windows.Forms.TextBox iptPassword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel colToolbar;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Panel colAdmin;
        private System.Windows.Forms.ComboBox iptisAdmin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.BindingSource empresasBindingSource;
        private System.Windows.Forms.BindingSource contactoBindingSource;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Panel colPwd2;
        private System.Windows.Forms.TextBox iptPasswordRepeat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblErrorsPwd1;
        private System.Windows.Forms.Label ErrorlblUsername;
        private System.Windows.Forms.Label ErrorlblAdmin;
        private System.Windows.Forms.Label ErrorlblPassword;
        private System.Windows.Forms.Label ErrorlblRepeatPassword;
        private System.Windows.Forms.Label lblErrorsPwd4;
        private System.Windows.Forms.Label lblErrorsPwd2;
        private System.Windows.Forms.Label lblErrorsPwd3;
        private System.Windows.Forms.Label lblErrorsPwd5;
        private System.Windows.Forms.Label lblErrorsGeral;
    }
}