﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace userContacts
{
    public partial class frmContacto : Form
    {
        private Contacto currentContacto;
        private bool novo;
        private bool admin;
        public frmContacto(Contacto current = null)
        {
            InitializeComponent();
            currentContacto = current;
            admin = frmPanel.loggedIn.Admin;

            iniciarUser();
            iniciarCampos();
            checkEmptyGrids();
            resizeDataGrid();

            preencherEmpresas();

            configurarComunicacoes();
            preencherComunicacoes();
        }

        private void iniciarUser()
        {
            DAL ligacao = new DAL();
            if (currentContacto == null)
            {
                currentContacto = new Contacto();
                iptUserCriacao.Text = frmPanel.loggedIn.username;
                dtDataCriacao.Value = currentContacto.dataCriacao;
                iptTitulo.Text = "Senhor";
                iptVisibilidade.Text = "Publico";
                novo = true;
            }
            else
            {
                iptTitulo.Text = currentContacto.titulo;
                iptNome.Text = currentContacto.nome;
                iptVisibilidade.Text = currentContacto.publico ? "Publico" : "Privado";
                iptMorada.Text = currentContacto.morada;
                iptNif.Text = currentContacto.nif;
                iptUserCriacao.Text = currentContacto.username;
                iptUserMod.Text = currentContacto.modUsername;
                dtDataCriacao.Value = currentContacto.dataCriacao;
                dtUserMod.Value = currentContacto.modData;
                ligacao.getEmpresas(currentContacto);
                ligacao.getComunicacoes(currentContacto);
                novo = false;
            }

        }

        private void backHome()
        {
            this.Hide();
            frmHome home = new frmHome();
            home.MdiParent = this.MdiParent;
            home.Dock = DockStyle.Fill;
            home.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            home.Show();
            this.Dispose();
            this.Close();
        }
        
        private void checkEmptyGrids()
        {
            if (currentContacto.Empresas.Count == 0) {
                Empresa nova = new Empresa();
                nova.Valor = "";
                currentContacto.Empresas.Add(nova);
            }

            if (currentContacto.Comunicacoes.Count == 0)
            {
                Comunicacao comun = new Comunicacao();
                comun.Id = "1";
                comun.Valor = "";
                currentContacto.Comunicacoes.Add(comun);
            }
        }

        private void configurarComunicacoes()
        {
            DataGridViewComboBoxCell tipos = new DataGridViewComboBoxCell();
            tipos.DataSource = new BindingSource(frmPanel.Tcomunicacao, null);
            tipos.ValueMember = "Id";
            tipos.DisplayMember = "Valor";
            tipos.FlatStyle = FlatStyle.Flat;

            DataGridViewColumn colunaCopy = new DataGridViewColumn(tipos);

            dtgComunicacao.Columns.Add(colunaCopy);
            dtgComunicacao.Columns[0].HeaderText = "Tipo";
            dtgComunicacao.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dtgComunicacao.Columns[0].Width = dtgComunicacao.Width / 3;

            dtgComunicacao.DataSource = null;
            dtgComunicacao.DataSource = currentContacto.Comunicacoes;

        }

        private void resizeDataGrid()
        {
            centerPanel.Width = this.Width - 200;
            centerPanel.Height = this.Height - 100;
            centerPanel.Location = new Point(100, 50);
            colTitulo.Width = colTitulo.Parent.Width / 4;
            colNome.Width = colNome.Parent.Width / 4 * 2;
            colEstado.Width = colEstado.Parent.Width / 4;

            colEmpresas.Width = colEmpresas.Parent.Width / 2;
            colContactos.Width = colContactos.Parent.Width / 2;

            colMorada.Width = colMorada.Parent.Width / 3 * 2;
            colNIF.Width = colNIF.Parent.Width / 3;

            colDataCriacao.Width = colDataCriacao.Parent.Width / 2;
            colDataModificacao.Width = colDataModificacao.Parent.Width / 2;

            colToolbar.Width = colToolbar.Parent.Width;

            refreshComboBox();
        }

        private void preencherEmpresas()
        {
            dtgEmpresas.DataSource = null;
            dtgEmpresas.DataSource = currentContacto.Empresas;

            dtgEmpresas.Columns["Id"].Visible = false;
            dtgEmpresas.Columns["Valor"].HeaderText = "Nome";
        }

        private void preencherComunicacoes()
        {
            dtgComunicacao.DataSource = null;
            dtgComunicacao.DataSource = currentContacto.Comunicacoes;

            foreach (DataGridViewRow item in dtgComunicacao.Rows)
            {
                item.Cells[0].Value = item.Cells["Id"].Value;
            }

            dtgComunicacao.Columns["Id"].Visible = false;
            dtgComunicacao.Columns["Valor"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dtgComunicacao.Columns["Valor"].HeaderText = "Contacto";
        }

        private void refreshComboBox()
        {
            foreach (DataGridViewRow item in dtgComunicacao.Rows)
            {
                item.Cells[0].Value = item.Cells["Id"].Value;
            }
        }

        private int validarCampos()
        {
            if (string.IsNullOrEmpty(iptTitulo.Text)) { return 99; }
            if (string.IsNullOrEmpty(iptNome.Text)) { return 99; }
            if (string.IsNullOrEmpty(iptVisibilidade.Text)) { return 99; }
            if (string.IsNullOrEmpty(iptMorada.Text)) { return 99; }
            if (string.IsNullOrEmpty(iptNif.Text)) { return 99; }
            foreach(Comunicacao com in this.currentContacto.Comunicacoes)
            {
                if (!string.IsNullOrEmpty(frmPanel.Tcomunicacao.Find(tc => tc.Id == com.Id).regex)) { 
                    if(!Regex.IsMatch(com.Valor, frmPanel.Tcomunicacao.Find(tc => tc.Id == com.Id).regex, RegexOptions.IgnoreCase))
                    {
                        lblErrors.Visible = true;
                        lblErrors.Text = string.Format("{0} não é um {1} válido.", com.Valor, frmPanel.Tcomunicacao.Find(tc => tc.Id == com.Id).Valor.ToLower());
                        return 100;
                    }
                }
            } 
            return 0;
        }

        private void iniciarCampos()
        {
            bool canEdit = currentContacto.username == frmPanel.loggedIn.username || admin || novo ? true : false;

            btnSave.Visible = canEdit;
            btnSave.Text = novo ? "Adicionar" : "Guardar";

            lblErrors.Visible = false;
            colDataCriacao.Visible = !novo;
            colDataModificacao.Visible = !novo;
            btnDelete.Visible = canEdit;
            btnAddContacto.Visible = canEdit;
            btnAddEmpresa.Visible = canEdit;
            btnDeleteContacto.Visible = canEdit;
            btnDeleteEmpresa.Visible = canEdit;
        }

        private void FrmMeusContactos_ResizeEnd(object sender, EventArgs e)
        {
            resizeDataGrid();
        }

        private void btnDeleteEmpresa_Click(object sender, EventArgs e)
        {
            if(dtgEmpresas.SelectedRows.Count != 0)
            {
                currentContacto.Empresas.RemoveAll(found => found.Valor == ((Empresa)dtgEmpresas.SelectedRows[0].DataBoundItem).Valor);
                preencherEmpresas();
            }
        }

        private void btnAddEmpresa_Click(object sender, EventArgs e)
        {
            if(dtgEmpresas.Rows.Count == 0 || (string)dtgEmpresas["Valor", 0].Value != "")
            {
                Empresa nova = new Empresa();
                nova.Valor = "";
                currentContacto.Empresas.Reverse();
                currentContacto.Empresas.Add(nova);
                currentContacto.Empresas.Reverse();
                preencherEmpresas();
            }
            dtgEmpresas.Rows[0].Selected = true;
            dtgEmpresas.CurrentCell = dtgEmpresas.Rows[0].Cells["Valor"];
            dtgEmpresas.Focus();
            dtgEmpresas.FirstDisplayedScrollingRowIndex = 0;
            dtgEmpresas.BeginEdit(true);
        }

        private void BtnDeleteContacto_Click(object sender, EventArgs e)
        {
            if (dtgComunicacao.SelectedRows.Count != 0)
            {
                currentContacto.Comunicacoes.RemoveAll(found => found.Valor == ((Comunicacao)dtgComunicacao.SelectedRows[0].DataBoundItem).Valor);
                preencherComunicacoes();
            }
        }

        private void BtnAddContacto_Click(object sender, EventArgs e)
        {
            if (dtgComunicacao.Rows.Count == 0 || (string)dtgComunicacao["Valor", 0].Value != "")
            {
                Comunicacao nova = new Comunicacao();
                nova.Id = "1";
                nova.Valor = "";
                currentContacto.Comunicacoes.Reverse();
                currentContacto.Comunicacoes.Add(nova);
                currentContacto.Comunicacoes.Reverse();
                preencherComunicacoes();
            }
            dtgComunicacao.Rows[0].Selected = true;
            dtgComunicacao.CurrentCell = dtgComunicacao.Rows[0].Cells["Valor"];
            dtgComunicacao.Focus();
            dtgComunicacao.FirstDisplayedScrollingRowIndex = 0;
            dtgComunicacao.BeginEdit(true);
        }

        private void DtgComunicacao_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            dtgComunicacao.SelectedRows[0].Cells["Id"].Value = dtgComunicacao.SelectedRows[0].Cells[0].Value;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (validarCampos() == 0) { 
                DAL ligacao = new DAL();
                currentContacto.titulo = iptTitulo.Text;
                currentContacto.nome = iptNome.Text;
                currentContacto.publico = iptVisibilidade.Text == "Publico" ? true : false;
                currentContacto.morada = iptMorada.Text;
                currentContacto.nif = iptNif.Text;
                currentContacto.username = iptUserCriacao.Text;
                currentContacto.modUsername = frmPanel.loggedIn.username;
                currentContacto.modData = DateTime.Now;
                if(this.novo)
                {
                    ligacao.addContact(currentContacto);
                    foreach (Empresa emp in currentContacto.Empresas)
                    {
                        ligacao.addEmpresa(emp, currentContacto.ID);
                    }

                    foreach (Comunicacao com in currentContacto.Comunicacoes)
                    {
                        ligacao.addComunicacao(com, currentContacto.ID);
                    }
                    Helper.launchLogger("Contacto adicionado", frmPanel.loggedIn, currentContacto);
                }
                else
                {
                    ligacao.atualizarContact(currentContacto);

                    ligacao.deleteComunicacoes(currentContacto);
                    ligacao.deleteEmpresas(currentContacto);
                    foreach(Empresa emp in currentContacto.Empresas)
                    {
                        ligacao.addEmpresa(emp, currentContacto.ID);
                    }
                    foreach (Comunicacao com in currentContacto.Comunicacoes)
                    {
                        ligacao.addComunicacao(com, currentContacto.ID);
                    }
                    Helper.launchLogger("Contacto editado", frmPanel.loggedIn, currentContacto);
                }
                backHome();
            } else if(validarCampos() == 99) {
                lblErrors.Visible = true;
                lblErrors.Text = Helper.listaErros[4];
            } else if (validarCampos() > 0 && validarCampos() < 98)
            {
                lblErrors.Visible = true;
                lblErrors.Text = Helper.listaErros[12];
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            backHome();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string msg = string.Format(Helper.listaErros[11], currentContacto.nome);
            if (Helper.getUserConfirmation(msg)) { 
                DAL ligacao = new DAL();

                ligacao.deleteContact(currentContacto);
                ligacao.deleteEmpresas(currentContacto);
                ligacao.deleteComunicacoes(currentContacto);
                Helper.launchLogger("Contacto apagado", frmPanel.loggedIn, currentContacto);

                backHome();
            }
        }
    }
}
