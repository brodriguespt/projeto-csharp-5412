﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace userContacts
{
    public static class Helper
    {
        private const string salt = "b0oO0Oo0bs";
        public static Dictionary<byte,string> listaErros = new Dictionary<byte, string> { 
        { 1, "Username inválido."},
        { 2, "Password inválida."},
        { 3, "Login incorrecto."},
        { 4, "Por favor preencha todos os campos."},
        { 5, "Password inválida."},
        { 6, "Username inválido, não pode conter espaços."},
        { 7, "Username já existe."},
        { 8, "Sessão iniciada como {0}"},
        { 9, "Quer fazer logout?"},
        { 10, "Deseja apagar o utilizador {0}?"},
        { 11, "Deseja apagar o contacto {0}?"},
        { 12, "12"},
        { 13, "13"},
        { 14, "14"}};

        public static DateTime getTimeforsql(DateTime dataHora)
        {
            DateTime horaConvertida = new DateTime(dataHora.Year, dataHora.Month, dataHora.Day, dataHora.Hour, dataHora.Minute, dataHora.Second);

            return horaConvertida;
        }

        public static string EncryptPass(string valor)
        {
            if (string.IsNullOrEmpty(valor))
                return string.Empty;
            valor += salt;
            byte[] data = System.Text.Encoding.ASCII.GetBytes(valor);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);

            return Convert.ToBase64String(data);
        }

        public static bool validarComprimento(string password)
        {
            return password.Length >= 6;
        }

        public static bool validarDigitos(string password)
        {
            bool temDigit = false;
            
            foreach (char c in password)
            {
                if (char.IsDigit(c)) temDigit = true;
            }

            return temDigit;
        }

        public static bool naotemEspacos(string user)
        {
            if(user.Contains(" ")) { return false; }
            return true;
        }

        public static bool userExists(string compare)
        {
            DAL ligacao = new DAL();

            return ligacao.checkUser(compare);
        }

        public static bool getUserConfirmation(string message)
        {
            DialogResult popup = MessageBox.Show(message, "Confirmar", MessageBoxButtons.YesNo);
            if (popup == DialogResult.Yes)
            {
                return true;
            }
            return false;
        }
        
        public static void launchLogger(string loggedAction, User loggedUser, Contacto contact = null)
        {
            string logString = string.Empty;
            DAL ligacao = new DAL();
            if(loggedAction == "Contacto adicionado" || loggedAction == "Contacto editado" || loggedAction == "Contacto apagado")
            {
                string visibilidade = contact.publico ? "Publico" : "Privado";
                logString = string.Format(loggedAction + " por: " + contact.modUsername + " | Titulo: " + contact.titulo + " | Nome: " + contact.nome + " | Visibilidade: " + visibilidade + " | Empresas : ");
                foreach(Empresa emp in contact.Empresas)
                {
                    logString += string.Format(emp.Valor + "; ");
                }
                logString += " | Contactos: ";
                foreach(Comunicacao com in contact.Comunicacoes)
                {
                    logString += string.Format(com.Valor + "; ");
                }
            }else
            {
                logString = loggedAction + " por " + frmPanel.loggedIn.username + ".";
            }
            ligacao.addLog(loggedUser.username, loggedAction, logString, string.Empty);
        }
    }
}
