﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace userContacts
{
    public class BaseType
    {
        public string Id { get; set; }
        public string Valor { get; set; }
    }

    public class Empresa : BaseType
    {
        public Empresa() { Id = Guid.NewGuid().ToString(); }
    }
    public class Comunicacao : BaseType
    { }
    public class TipoComunicacao : BaseType
    {
        public string regex { get; set; }
    }
}
