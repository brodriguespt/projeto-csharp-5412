﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace userContacts
{
    public partial class frmMudarPass : Form
    {
        private User currentUser;
        public frmMudarPass(User selecionado = null)
        {
            InitializeComponent();
            currentUser = selecionado;

            iniciarUser();
            resizeDataGrid();
            iniciarCampos();
        }

        private void iniciarUser()
        {
            DAL ligacao = new DAL();
            ligacao.getUser(currentUser);
        }

        private void backHome()
        {
            this.Hide();
            frmUserList home = new frmUserList();
            home.MdiParent = this.MdiParent;
            home.Dock = DockStyle.Fill;
            home.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            home.Show();
            this.Dispose();
            this.Close();
        }

        private void resizeDataGrid()
        {
            centerPanel.Width = this.Width - 200;
            centerPanel.Height = this.Height - 100;
            centerPanel.Location = new Point(100, 50);

            colPwd1.Width = colPwd1.Parent.Width / 2;
            colPwd2.Width = colPwd2.Parent.Width / 2;

            colToolbar.Width = colToolbar.Parent.Width;
        }

        private int validarCampos()
        {
            if (!string.IsNullOrEmpty(iptPassword.Text))
            {
                if (iptPassword.Text != iptPasswordRepeat.Text || !Helper.validarComprimento(iptPassword.Text) || !Helper.validarDigitos(iptPassword.Text)) { return 2; }
            }
            return 0;
        }

        private void iniciarCampos()
        {
            bool canEdit = currentUser.username == frmPanel.loggedIn.username || frmPanel.loggedIn.Admin ? true : false;
            bool canDelete = frmPanel.loggedIn.Admin && currentUser.username != frmPanel.loggedIn.username ? true : false;
            btnSave.Visible = canEdit;
            btnSave.Text = "Guardar";
        }

        private void FrmMeusContactos_ResizeEnd(object sender, EventArgs e)
        {
            resizeDataGrid();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            int validacao = validarCampos();
            if (validacao == 0)
            {
                DAL ligacao = new DAL();
                currentUser.Password = Helper.EncryptPass(iptPasswordRepeat.Text);
                ligacao.atualizarUser(currentUser);
                Helper.launchLogger("Password alterada", currentUser);
                backHome();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            backHome();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DAL ligacao = new DAL();

            ligacao.deleteUser(currentUser);
            backHome();
        }

        private void iptPassword_TextChanged(object sender, EventArgs e)
        {
            lblErrorsPwd4.ForeColor = Helper.validarComprimento(iptPassword.Text) ? Color.Lime : Color.OrangeRed;
            lblErrorsPwd2.ForeColor = Helper.validarDigitos(iptPassword.Text) ? Color.Lime : Color.OrangeRed;
        }

        private void iptPasswordRepeat_TextChanged(object sender, EventArgs e)
        {
            lblErrorsPwd5.ForeColor = iptPassword.Text == iptPasswordRepeat.Text ? Color.Lime : Color.OrangeRed;
        }

        private void iptPassword_Enter(object sender, EventArgs e)
        {
            lblErrorsPwd1.Visible = true;
            lblErrorsPwd4.Visible = true;
            lblErrorsPwd3.Visible = true;
            lblErrorsPwd2.Visible = true;
        }

        private void iptPasswordRepeat_Enter(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(iptPassword.Text))
            {
                lblErrorsPwd5.Visible = true;
                lblErrorsPwd5.BringToFront();
            }
        }
    }
}
