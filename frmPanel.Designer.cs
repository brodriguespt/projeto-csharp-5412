﻿namespace userContacts
{
    partial class frmPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnHome = new System.Windows.Forms.Button();
            this.btnMeusContactos = new System.Windows.Forms.Button();
            this.btnAddContacto = new System.Windows.Forms.Button();
            this.btnAllUsers = new System.Windows.Forms.Button();
            this.btnAddUser = new System.Windows.Forms.Button();
            this.btnMudarPassword = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.userInfo = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.userInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.btnHome);
            this.panel1.Controls.Add(this.btnMeusContactos);
            this.panel1.Controls.Add(this.btnAddContacto);
            this.panel1.Controls.Add(this.btnAllUsers);
            this.panel1.Controls.Add(this.btnAddUser);
            this.panel1.Controls.Add(this.btnMudarPassword);
            this.panel1.Controls.Add(this.btnLogout);
            this.panel1.Controls.Add(this.userInfo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 561);
            this.panel1.TabIndex = 1;
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(128)))), ((int)(((byte)(188)))));
            this.btnHome.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnHome.Location = new System.Drawing.Point(0, 149);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(300, 50);
            this.btnHome.TabIndex = 3;
            this.btnHome.Text = "Home";
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // btnMeusContactos
            // 
            this.btnMeusContactos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(128)))), ((int)(((byte)(188)))));
            this.btnMeusContactos.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnMeusContactos.FlatAppearance.BorderSize = 0;
            this.btnMeusContactos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMeusContactos.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMeusContactos.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnMeusContactos.Location = new System.Drawing.Point(0, 199);
            this.btnMeusContactos.Name = "btnMeusContactos";
            this.btnMeusContactos.Size = new System.Drawing.Size(300, 50);
            this.btnMeusContactos.TabIndex = 10;
            this.btnMeusContactos.Text = "Meus Contactos";
            this.btnMeusContactos.UseVisualStyleBackColor = false;
            this.btnMeusContactos.Click += new System.EventHandler(this.btnMeusContactos_Click);
            // 
            // btnAddContacto
            // 
            this.btnAddContacto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(128)))), ((int)(((byte)(188)))));
            this.btnAddContacto.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAddContacto.FlatAppearance.BorderSize = 0;
            this.btnAddContacto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddContacto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddContacto.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnAddContacto.Location = new System.Drawing.Point(0, 249);
            this.btnAddContacto.Name = "btnAddContacto";
            this.btnAddContacto.Size = new System.Drawing.Size(300, 50);
            this.btnAddContacto.TabIndex = 9;
            this.btnAddContacto.Text = "Adicionar Contacto";
            this.btnAddContacto.UseVisualStyleBackColor = false;
            this.btnAddContacto.Click += new System.EventHandler(this.BtnAddContacto_Click);
            // 
            // btnAllUsers
            // 
            this.btnAllUsers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(100)))), ((int)(((byte)(150)))));
            this.btnAllUsers.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAllUsers.FlatAppearance.BorderSize = 0;
            this.btnAllUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAllUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAllUsers.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnAllUsers.Location = new System.Drawing.Point(0, 299);
            this.btnAllUsers.Name = "btnAllUsers";
            this.btnAllUsers.Size = new System.Drawing.Size(300, 50);
            this.btnAllUsers.TabIndex = 12;
            this.btnAllUsers.Text = "Utilizadores";
            this.btnAllUsers.UseVisualStyleBackColor = false;
            this.btnAllUsers.Click += new System.EventHandler(this.BtnAllUsers_Click);
            // 
            // btnAddUser
            // 
            this.btnAddUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(100)))), ((int)(((byte)(150)))));
            this.btnAddUser.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAddUser.FlatAppearance.BorderSize = 0;
            this.btnAddUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddUser.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnAddUser.Location = new System.Drawing.Point(0, 349);
            this.btnAddUser.Name = "btnAddUser";
            this.btnAddUser.Size = new System.Drawing.Size(300, 50);
            this.btnAddUser.TabIndex = 11;
            this.btnAddUser.Text = "Adicionar Utilizador";
            this.btnAddUser.UseVisualStyleBackColor = false;
            this.btnAddUser.Click += new System.EventHandler(this.BtnAddUser_Click);
            // 
            // btnMudarPassword
            // 
            this.btnMudarPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(100)))), ((int)(((byte)(150)))));
            this.btnMudarPassword.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnMudarPassword.FlatAppearance.BorderSize = 0;
            this.btnMudarPassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMudarPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMudarPassword.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnMudarPassword.Location = new System.Drawing.Point(0, 399);
            this.btnMudarPassword.Name = "btnMudarPassword";
            this.btnMudarPassword.Size = new System.Drawing.Size(300, 50);
            this.btnMudarPassword.TabIndex = 13;
            this.btnMudarPassword.Text = "Mudar Password";
            this.btnMudarPassword.UseVisualStyleBackColor = false;
            this.btnMudarPassword.Visible = false;
            this.btnMudarPassword.Click += new System.EventHandler(this.btnMudarPassword_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(80)))), ((int)(((byte)(97)))));
            this.btnLogout.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnLogout.Location = new System.Drawing.Point(0, 449);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(300, 50);
            this.btnLogout.TabIndex = 8;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // userInfo
            // 
            this.userInfo.Controls.Add(this.lblTitle);
            this.userInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.userInfo.Location = new System.Drawing.Point(0, 499);
            this.userInfo.Name = "userInfo";
            this.userInfo.Size = new System.Drawing.Size(300, 62);
            this.userInfo.TabIndex = 7;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(255)))));
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(18)))), ((int)(((byte)(11)))));
            this.lblTitle.Location = new System.Drawing.Point(16, 23);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(155, 18);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Sessão iniciada como";
            // 
            // frmPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DodgerBlue;
            this.ClientSize = new System.Drawing.Size(1264, 561);
            this.Controls.Add(this.panel1);
            this.IsMdiContainer = true;
            this.Name = "frmPanel";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Panel";
            this.EnabledChanged += new System.EventHandler(this.btnHome_Click);
            this.panel1.ResumeLayout(false);
            this.userInfo.ResumeLayout(false);
            this.userInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Panel userInfo;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button btnMeusContactos;
        private System.Windows.Forms.Button btnAddContacto;
        private System.Windows.Forms.Button btnAddUser;
        private System.Windows.Forms.Button btnAllUsers;
        private System.Windows.Forms.Button btnMudarPassword;
    }
}