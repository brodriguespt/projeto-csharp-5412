﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Data;

namespace userContacts
{
    public class DAL
    {
        private OleDbConnection _conexao;

        public DAL()
        {
            _conexao = new OleDbConnection();
            _conexao.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=contactos.mdb";
        }

        #region users
        public void addUser(User _user)
        {
            if (_conexao.State != ConnectionState.Open)
                _conexao.Open();

            OleDbCommand command = new OleDbCommand();
            try
            {
                command.Connection = _conexao;

                command.CommandText = "INSERT INTO[Login] ([username],[password],[admin]) VALUES(@username,@password,@admin)";
                command.Parameters.AddWithValue("@username", _user.username);
                command.Parameters.AddWithValue("@password", _user.Password);
                command.Parameters.AddWithValue("@admin", _user.Admin);

                int linhas = command.ExecuteNonQuery();
            }
            finally
            {
                _conexao.Close();
            }
        }
        
        public List<User> getAllUsers()
        {
            List<User> userslist = new List<User>();
            try
            {
                if (_conexao.State != ConnectionState.Open)
                    _conexao.Open();

                OleDbCommand command = new OleDbCommand();
                command.Connection = _conexao;
                command.CommandText = "SELECT * FROM [Login]";

                OleDbDataReader dataR = command.ExecuteReader();
                if (dataR.HasRows)
                {
                    while (dataR.Read())
                    {
                        User _utilizador = new User();
                        _utilizador.username = dataR.GetString(0);
                        _utilizador.Password = dataR.GetString(1);
                        _utilizador.Admin = dataR.GetBoolean(2);
                        userslist.Add(_utilizador);
                    }
                }
            }
            finally
            {
                _conexao.Close();
            }
            return userslist;
        }

        public User getUser(User current)
        {
            try
            {
                if (_conexao.State != ConnectionState.Open)
                    _conexao.Open();

                OleDbCommand command = new OleDbCommand();
                command.Connection = _conexao;

                command.CommandText = "SELECT * FROM [Login] WHERE [Username] = @user";
                command.Parameters.AddWithValue("@user", current.username);

                OleDbDataReader dataR = command.ExecuteReader();
                if (dataR.HasRows)
                {
                    dataR.Read();
                    current.username = dataR.GetString(0);
                    current.Password = dataR.GetString(1);
                    current.Admin = dataR.GetBoolean(2);
                }
            }
            finally
            {
                _conexao.Close();
            }
            return current;
        }

        public void atualizarUser(User _user)
        {
            if (_conexao.State != ConnectionState.Open)
                _conexao.Open();

            OleDbCommand command = new OleDbCommand();
            try
            {
                command.Connection = _conexao;

                command.CommandText = "UPDATE [Login] SET [password]=@password, [admin]=@admin WHERE [username]=@username";

                command.Parameters.AddWithValue("@password", _user.Password);
                command.Parameters.AddWithValue("@admin", _user.Admin);
                command.Parameters.AddWithValue("@username", _user.username);

                int linhas = command.ExecuteNonQuery();
            }
            finally
            {
                _conexao.Close();
            }
        }

        public void deleteUser(User _user)
        {
            if (_conexao.State != ConnectionState.Open)
                _conexao.Open();

            OleDbCommand command = new OleDbCommand();
            try
            {
                command.Connection = _conexao;

                command.CommandText = "DELETE FROM [Login] WHERE [username] = @user";
                command.Parameters.AddWithValue("@id", _user.username);


                int linhas = command.ExecuteNonQuery();
            }
            finally
            {
                _conexao.Close();
            }
        }

        public User checkLogin(string utilizador, string password)
        {
            try
            {
                if (_conexao.State != ConnectionState.Open)
                    _conexao.Open();

                OleDbCommand command = new OleDbCommand();
                command.Connection = _conexao;
                command.CommandText = "SELECT [username],[password],[admin] FROM [Login] WHERE [username] = @u AND [password] = @p";
                command.Parameters.AddWithValue("@u", utilizador);
                command.Parameters.AddWithValue("@p", password);

                OleDbDataReader dataR = command.ExecuteReader();
                if (dataR.HasRows)
                {
                    User _utilizador = new User();
                    dataR.Read();
                    _utilizador.username = dataR.GetString(0);
                    _utilizador.Password = dataR.GetString(1);
                    _utilizador.Admin = dataR.GetBoolean(2);

                    _conexao.Close();
                    return _utilizador;
                }
                else
                {
                    _conexao.Close();
                    return null;
                }
            }
            finally
            {
                _conexao.Close();
            }
        }
<<<<<<< HEAD

        public bool checkUser(string utilizador)
        {
            try
            {
                if (_conexao.State != ConnectionState.Open)
                    _conexao.Open();

                OleDbCommand command = new OleDbCommand();
                command.Connection = _conexao;
                command.CommandText = "SELECT [username] FROM [Login] WHERE [username] = @u";
                command.Parameters.AddWithValue("@u", utilizador);

                OleDbDataReader dataR = command.ExecuteReader();
                if (dataR.HasRows)
                {
                    _conexao.Close();
                    return true;
                }
                else
                {
                    _conexao.Close();
                    return false;
                }
            }
            finally
            {
                _conexao.Close();
            }
        }
=======
>>>>>>> a50100bb64e52c95beb4ff36b7510a01b0aca082
        #endregion users

        #region contactos
        public List<Contacto> getPublicContacts()
        {
            List<Contacto> listagem = new List<Contacto>();
            try { 
                if (_conexao.State != ConnectionState.Open)
                    _conexao.Open();

                OleDbCommand command = new OleDbCommand();
                command.Connection = _conexao;
                command.CommandText = "SELECT * FROM [Contacto] WHERE [Contacto.publico]=true";

                OleDbDataReader dataR = command.ExecuteReader();
                if (dataR.HasRows)
                {
                    while (dataR.Read())
                    {
                        Contacto _contacto = new Contacto();
                        _contacto.ID = dataR.GetString(0);
                        _contacto.username = dataR.GetString(1);
                        _contacto.nome = dataR.GetString(2);
                        _contacto.titulo = dataR.GetString(3);
                        _contacto.morada = dataR.GetString(4);
                        _contacto.nif = dataR.GetString(5);
                        _contacto.publico = dataR.GetBoolean(6);
                        _contacto.dataCriacao = dataR.GetDateTime(7);
                        _contacto.modUsername = dataR.GetString(8);
                        _contacto.modData = dataR.GetDateTime(9);
                        listagem.Add(_contacto);
                    }
                }
            }
            finally{
                _conexao.Close();
            }
            return listagem;
        }

        public List<Contacto> getUserContacts(User utilizador)
        {
            List<Contacto> listagem = new List<Contacto>();
            try
            {
                if (_conexao.State != ConnectionState.Open)
                    _conexao.Open();

                OleDbCommand command = new OleDbCommand();
                command.Connection = _conexao;
                command.CommandText = "SELECT * FROM [Contacto] WHERE [Contacto.username]=@user";
                command.Parameters.AddWithValue("@user", utilizador.username);

                OleDbDataReader dataR = command.ExecuteReader();
                if (dataR.HasRows)
                {
                    while (dataR.Read())
                    {
                        Contacto _contacto = new Contacto();
                        _contacto.ID = dataR.GetString(0);
                        _contacto.username = dataR.GetString(1);
                        _contacto.nome = dataR.GetString(2);
                        _contacto.titulo = dataR.GetString(3);
                        _contacto.morada = dataR.GetString(4);
                        _contacto.nif = dataR.GetString(5);
                        _contacto.publico = dataR.GetBoolean(6);
                        _contacto.dataCriacao = dataR.GetDateTime(7);
                        _contacto.modUsername = dataR.GetString(8);
                        _contacto.modData = dataR.GetDateTime(9);
                        listagem.Add(_contacto);
                    }
                }
            }
            finally
            {
                _conexao.Close();
            }
            return listagem;
        }

        public void addContact(Contacto _contacto)
        {
            if (_conexao.State != ConnectionState.Open)
                _conexao.Open();

            OleDbCommand command = new OleDbCommand();
            try { 
                command.Connection = _conexao;

                command.CommandText = "INSERT INTO [Contacto] ([idContacto],[username],[nome],[titulo],[morada],[NIF],[publico],[dataCriacao],[modUsername],[modData]) VALUES(@id,@username,@nome,@titulo,@morada,@nif,@publico,@dataCriacao,@modUsername,@modData)";

                command.Parameters.AddWithValue("@id", _contacto.ID);
                command.Parameters.AddWithValue("@username", _contacto.username);
                command.Parameters.AddWithValue("@nome", _contacto.nome);
                command.Parameters.AddWithValue("@titulo", _contacto.titulo);
                command.Parameters.AddWithValue("@morada", _contacto.morada);
                command.Parameters.AddWithValue("@nif", _contacto.nif);
                command.Parameters.AddWithValue("@publico", _contacto.publico);
                command.Parameters.AddWithValue("@dataCriacao", Helper.getTimeforsql(_contacto.dataCriacao));
                command.Parameters.AddWithValue("@modUsername", _contacto.modUsername);
                command.Parameters.AddWithValue("@modData", Helper.getTimeforsql(_contacto.modData));

                int linhas = command.ExecuteNonQuery();
            }
            finally{
                _conexao.Close();
            }
        }

        public void atualizarContact(Contacto _contacto)
        {
            if (_conexao.State != ConnectionState.Open)
                _conexao.Open();

            OleDbCommand command = new OleDbCommand();
            try
            {
                command.Connection = _conexao;

                command.CommandText = "UPDATE [Contacto] SET [username]=@username, [nome]=@nome, [titulo]=@titulo, [morada]=@morada, [NIF]=@nif, [publico]=@publico, [dataCriacao]=@dataCriacao, [modUsername]=@modUsername, [modData]=@modData WHERE [idContacto]=@id";
                
                command.Parameters.AddWithValue("@username", _contacto.username);
                command.Parameters.AddWithValue("@nome", _contacto.nome);
                command.Parameters.AddWithValue("@titulo", _contacto.titulo);
                command.Parameters.AddWithValue("@morada", _contacto.morada);
                command.Parameters.AddWithValue("@nif", _contacto.nif);
                command.Parameters.AddWithValue("@publico", _contacto.publico);
                command.Parameters.AddWithValue("@dataCriacao", Helper.getTimeforsql(_contacto.dataCriacao));
                command.Parameters.AddWithValue("@modUsername", _contacto.modUsername);
                command.Parameters.AddWithValue("@modData", Helper.getTimeforsql(_contacto.modData));
                command.Parameters.AddWithValue("@id", _contacto.ID);

                int linhas = command.ExecuteNonQuery();
            }
            finally
            {
                _conexao.Close();
            }
        }

        public void deleteContact(Contacto _contacto)
        {
            if (_conexao.State != ConnectionState.Open)
                _conexao.Open();

            OleDbCommand command = new OleDbCommand();
            try
            {
                command.Connection = _conexao;

                command.CommandText = "DELETE FROM [Contacto] WHERE [idContacto] = @id";
                command.Parameters.AddWithValue("@id", _contacto.ID);


                int linhas = command.ExecuteNonQuery();
            }
            finally
            {
                _conexao.Close();
            }
        }
        #endregion contactos

        #region Empresas
        public void addEmpresa(Empresa emp, string idC)
        {
            if (_conexao.State != ConnectionState.Open)
                _conexao.Open();

            OleDbCommand command = new OleDbCommand();
            command.Connection = _conexao;
            try
            {
                command.CommandText = "INSERT INTO [Empresa] ([idContacto],[nome]) VALUES(@idC, @nome)";
                command.Parameters.AddWithValue("@idC", idC);
                command.Parameters.AddWithValue("@nome", emp.Valor);
                    
                int linhas = command.ExecuteNonQuery();
            }
            finally
            {
                _conexao.Close();
            }
        }

        public void deleteEmpresas(Contacto _contact)
        {
            if (_conexao.State != ConnectionState.Open)
                _conexao.Open();

            OleDbCommand command = new OleDbCommand();
            command.Connection = _conexao;
            try
            {
                command.CommandText = "DELETE FROM [Empresa] WHERE [idContacto] = @idC";
                command.Parameters.AddWithValue("@idC", _contact.ID);

                int linhas = command.ExecuteNonQuery();
            }
            finally
            {
                _conexao.Close();
            }
        }

        public void addEmpresas(Contacto _contacto)
        {
            if (_conexao.State != ConnectionState.Open)
                _conexao.Open();

            OleDbCommand command = new OleDbCommand();
            try
            {
                foreach(Empresa emp in _contacto.Empresas)
                {
                    command.Connection = _conexao;

                    if (!string.IsNullOrEmpty(emp.Valor))
                    {
                        command.CommandText = "INSERT INTO [Empresa] ([idContacto],[Nome]) VALUES(@idC, @nome)";
                        command.Parameters.AddWithValue("@idC", _contacto.ID);
                        command.Parameters.AddWithValue("@nome", emp.Valor);

                        int linhas = command.ExecuteNonQuery();
                    }
                }
            }
            finally
            {
                _conexao.Close();
            }
        }

        public void getEmpresas(Contacto _contacto)
        {
            try
            {
                if (_conexao.State != ConnectionState.Open)
                    _conexao.Open();

                OleDbCommand command = new OleDbCommand();
                command.Connection = _conexao;
                command.CommandText = "SELECT * FROM [Empresa] WHERE[Empresa.idContacto] = @id";
                command.Parameters.AddWithValue("@id", _contacto.ID);
                //
                OleDbDataReader dataR = command.ExecuteReader();
                if (dataR.HasRows)
                {
                    while (dataR.Read())
                    {
                        Empresa linha = new Empresa();
                        linha.Id = dataR.GetString(0);
                        linha.Valor = dataR.GetString(1);
                        _contacto.Empresas.Add(linha);
                    }
                }
            }
            finally
            {
                _conexao.Close();
            }
        }
        #endregion Empresas

        #region Comunicacoes

        public void addComunicacao(Comunicacao com, string idC)
        {
            if (_conexao.State != ConnectionState.Open)
                _conexao.Open();

            OleDbCommand command = new OleDbCommand();
            try
            {
                command.Connection = _conexao;

                if (!string.IsNullOrEmpty(com.Valor) && !string.IsNullOrEmpty(com.Id)) { 
                    command.CommandText = "INSERT INTO [Comunicacao] ([idContacto],[idTipo],[valor]) VALUES(@id, @idTipo, @valor)";
                    command.Parameters.AddWithValue("@id", idC);
                    command.Parameters.AddWithValue("@idTipo", com.Id);
                    command.Parameters.AddWithValue("@valor", com.Valor);

                    int linhas = command.ExecuteNonQuery();
                }
            }
            finally
            {
                _conexao.Close();
            }
        }

        public void deleteComunicacoes(Contacto _contacto)
        {
            if (_conexao.State != ConnectionState.Open)
                _conexao.Open();

            OleDbCommand command = new OleDbCommand();
            try
            {
                command.Connection = _conexao;

                command.CommandText = "DELETE FROM [Comunicacao] WHERE [idContacto] = @id";
                command.Parameters.AddWithValue("@id", _contacto.ID);
                
                int linhas = command.ExecuteNonQuery();
            }
            finally
            {
                _conexao.Close();
            }
        }

        public void addComunicacoes(Contacto _contacto)
        {
            if (_conexao.State != ConnectionState.Open)
                _conexao.Open();
            
            OleDbCommand command = new OleDbCommand();
            try
            {
                foreach (Comunicacao com in _contacto.Comunicacoes)
                {
                    command.Connection = _conexao;

                    if (!string.IsNullOrEmpty(com.Valor))
                    {
                        command.CommandText = "INSERT INTO [Comunicacao] ([idContacto],[idTipo],[valor]) VALUES(@id, @idTipo, @valor)";
                        command.Parameters.AddWithValue("@id", _contacto.ID);
                        command.Parameters.AddWithValue("@idTipo", com.Id);
                        command.Parameters.AddWithValue("@valor", com.Valor);

                        int linhas = command.ExecuteNonQuery();
                    }
                }
            }
            finally
            {
                _conexao.Close();
            }
        }

        public void getComunicacoes(Contacto _contacto)
        {
            try
            {
                if (_conexao.State != ConnectionState.Open)
                    _conexao.Open();

                OleDbCommand command = new OleDbCommand();
                command.Connection = _conexao;
                command.CommandText = "SELECT [idTipo],[valor] FROM [Comunicacao] WHERE [idContacto]=@id";
                command.Parameters.AddWithValue("@id", _contacto.ID);

                OleDbDataReader dataR = command.ExecuteReader();
                if (dataR.HasRows)
                {
                    while (dataR.Read())
                    {
                        Comunicacao linha = new Comunicacao();
                        linha.Id = dataR.GetString(0);
                        linha.Valor = dataR.GetString(1);
                        _contacto.Comunicacoes.Add(linha);
                    }
                }
            }
            finally
            {
                _conexao.Close();
            }
        }

        public List<TipoComunicacao> getTipoComunicacoes()
        {
            List<TipoComunicacao> listadeTipos = new List<TipoComunicacao>();
            try
            {
                if (_conexao.State != ConnectionState.Open)
                    _conexao.Open();

                OleDbCommand command = new OleDbCommand();
                command.Connection = _conexao;
                command.CommandText = "SELECT * FROM [TipoComunicacao]";

                OleDbDataReader dataR = command.ExecuteReader();
                if (dataR.HasRows)
                {
                    while (dataR.Read())
                    {
                        TipoComunicacao tipoCom = new TipoComunicacao();
                        tipoCom.Id = dataR.GetString(0);
                        tipoCom.Valor = dataR.GetString(1);
                        listadeTipos.Add(tipoCom);
                    }
                }
            }
            finally
            {
                _conexao.Close();
            }
            return listadeTipos;
        }
        #endregion Comunicacoes

    }
}
