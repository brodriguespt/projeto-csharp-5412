﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace userContacts
{
    public partial class frmMeusContactos : Form
    {
        public frmMeusContactos()
        {
            InitializeComponent();
            resizeDataGrid();
            DAL ligacao = new DAL();
            List<Contacto> meus = ligacao.getUserContacts(frmPanel.loggedIn);
            dataContactos.DataSource = meus;
            this.dataContactos.Columns["ID"].Visible = false;
            this.dataContactos.Columns["username"].Visible = false;
            this.dataContactos.Columns["modData"].Visible = false;
            this.dataContactos.Columns["publico"].Visible = false;
            this.dataContactos.Columns["modUsername"].Visible = false;
            this.dataContactos.Columns["dataCriacao"].Visible = false;
        }

        private void resizeDataGrid()
        {
            dataContactos.Width = this.Width - 200;
            dataContactos.Height = this.Height - 100;
            dataContactos.Location = new Point(100, 50);
        }

        private void DataContactos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Contacto selecionado = (Contacto)dataContactos.CurrentRow.DataBoundItem;
            this.Hide();
            frmContacto single = new frmContacto(selecionado);
            single.MdiParent = this.MdiParent;
            single.Dock = DockStyle.Fill;
            single.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            single.Show();
            this.Dispose();
            this.Close();
        }

        private void FrmMeusContactos_ResizeEnd(object sender, EventArgs e)
        {
            resizeDataGrid();
        }
    }
}
