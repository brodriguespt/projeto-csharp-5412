﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace userContacts
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
            lblUserError.Text = "";
            lblPasswordError.Text = "";
            lblLoginErrors.Text = "";
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            lblPasswordError.Text = "";
            lblUserError.Text = "";
            lblLoginErrors.Text = "";
            bool errors = false;
            if(string.IsNullOrEmpty(inputUser.Text)){ lblUserError.Text = Helper.listaErros[1]; errors = true; }
            if(string.IsNullOrEmpty(inputPassword.Text)){ lblPasswordError.Text = Helper.listaErros[2]; errors = true; }
            if (!errors)
            {
                User loggedIn = new User();
                DAL ligacao = new DAL();
                loggedIn = ligacao.checkLogin(inputUser.Text, Helper.EncryptPass(inputPassword.Text));
                if(loggedIn != null)
                {
                    this.Hide();
                    frmPanel main = new frmPanel(loggedIn);
                    main.ShowDialog();
                    this.Dispose();
                    this.Close();
                }
                else
                {
                    lblLoginErrors.Text = Helper.listaErros[3];
                }
            }
        }
    }
}
