﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace userContacts
{
    public class User
    {
        private string _password;
        private bool _admin = false;
        public string username { get; set; }

        public User()
        {
        }

        public User(string _username, string _password)
        {
            this.username = _username;
            this.Password = _password;
        }


        public string Password
        {
            get { return _password; }
            set { this._password = value; }
        }

        public bool Admin
        {
            get { return _admin; }
            set { this._admin = value; }
        }
    }
}
