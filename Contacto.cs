﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace userContacts
{
    public class Contacto
    {
        private string _idContacto;
        public string username { get; set; } = "";
        public string titulo { get; set; } = "";
        public string nome { get; set; } = "";
        public string morada { get; set; } = "";
        public string nif { get; set; } = "";
        public bool publico { get; set; } = false;
        public DateTime dataCriacao { get; set; } = DateTime.Now;
        public string modUsername { get; set; } = "";
        public DateTime modData { get; set; } = DateTime.Now;
        public List<Empresa> Empresas { get; set; }
        public List<Comunicacao> Comunicacoes { get; set; }

        public Contacto()
        {
            _idContacto = Guid.NewGuid().ToString();
            Empresas = new List<Empresa>();
            Comunicacoes = new List<Comunicacao>();
        }

        public Contacto(string username, string nome, string titulo, string morada, string nif, bool publico, DateTime dataCriacao, string modUsername, DateTime modData) : this()
        {
            this.username = username;
            this.nome = nome;
            this.titulo = titulo;
            this.morada = morada;
            this.nif = nif;
            this.publico = publico;
            this.dataCriacao = dataCriacao;
            this.modUsername = modUsername;
            this.modData = modData;
        }

        public string ID
        {
            get { return _idContacto; }
            set { this._idContacto = value; }
        }
    }
}
