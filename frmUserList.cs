﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace userContacts
{
    public partial class frmUserList : Form
    {
        public frmUserList()
        {
            InitializeComponent();
            resizeDataGrid();
            DAL ligacao = new DAL();
            List<User> meus = ligacao.getAllUsers();
            dataallUsers.DataSource = meus;
            dataallUsers.Columns["Password"].Visible = false;
        }

        private void DataContactos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            User selecionado = (User)dataallUsers.CurrentRow.DataBoundItem;
            this.Hide();
            frmUser single = new frmUser(selecionado);
            single.MdiParent = this.MdiParent;
            single.Dock = DockStyle.Fill;
            single.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            single.Show();
            this.Dispose();
            this.Close();
        }

        private void resizeDataGrid()
        {
            dataallUsers.Width = this.Width - 200;
            dataallUsers.Height = this.Height - 100;
            dataallUsers.Location = new Point(100, 50);
        }
        
        private void FrmMeusContactos_ResizeEnd(object sender, EventArgs e)
        {
            resizeDataGrid();
        }
    }
}
